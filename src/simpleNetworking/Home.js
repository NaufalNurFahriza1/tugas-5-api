import React from 'react';
import { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  View,
  FlatList,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
  Alert,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import { BASE_URL, TOKEN } from './url';
import { useIsFocused } from '@react-navigation/native';
import { TextInput } from 'react-native-gesture-handler';

const Home = ({ navigation, route }) => {
  const [dataMobil, setDataMobil] = useState([]);
  const isFocused = useIsFocused();
  const [modalVisible, setModalVisible] = useState(false);

  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [loading, setLoading] = useState(false);

  const getDataMobil = async () => {
    setLoading(true);
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
      });

      const result = await response.json();
      console.log('Success:', result);
      setDataMobil(result.items);
    } catch (error) {
      console.error('Error:', error);
    }
    setLoading(false);
    setTimeout(() => { }, 2000);
  };

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const postData = async () => {
    setLoading(true)

    if (!namaMobil || !totalKM || !hargaMobil) {
      setLoading(false);
      alert('Nama mobil, Total Kilometer dan Harga Mobil tidak boleh');
      return;
    }

    if (hargaMobil < 20000000) {
      setLoading(false);
      alert(
        'Harga mobil tidak boleh kosong dan tidak boleh dibawah 100 juta rupiah',
      );
      return;
    }

    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://gitlab.com/uploads/-/system/user/avatar/3683300/avatar.png?width=400',
      },
    ];

    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      Alert.alert('Alert Title', 'Data Mobil berhasil ditambahkan', [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => setModalVisible(false) },

      ])
    } catch (error) {
      console.error('Error:', error);
    }

    setLoading(false)
    setTimeout(() => {
    }, 2000)
  };

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Modal animationType="fade" transparent={true} visible={loading}>
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 20,
              borderRadius: 18,
              backgroundColor: '#FFFFFF',
            }}>
            <ActivityIndicator size="large" color="#00FF00" />
          </View>
        </View>
      </Modal>

      <Text
        style={{ fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000' }}>
        Home screen
      </Text>

      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('ScreenDetail', item)}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{ width: '90%', height: 100, resizeMode: 'contain' }}
                source={{ uri: item.unitImage }}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{ fontWeight: '700', fontSize: 14, color: '#000' }}>
                  Nama Mobil :
                </Text>
                <Text style={{ fontSize: 14, color: '#000' }}> {item.title}</Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{ fontWeight: '700', fontSize: 14, color: '#000' }}>
                  Total KM :
                </Text>
                <Text style={{ fontSize: 14, color: '#000' }}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{ fontWeight: '700', fontSize: 14, color: '#000' }}>
                  Harga Mobil :
                </Text>
                <Text style={{ fontSize: 14, color: '#000' }}>
                  {' '}
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      {/* Tombol Tambah data menggunakan AddData.js */}

      {/* <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          left: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity> */}

      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => setModalVisible(true)}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>

      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              paddingHorizontal: 10,
              paddingVertical: 10,
              borderRadius: 18,
              backgroundColor: '#FFFFFF',
              width: '85%',
              height: '65%',
              alignContent: 'center',
              justifyContent: 'center',

            }}>
            <TouchableOpacity
              onPress={() => setModalVisible(false)}
              style={{
                width: '10%',
                alignSelf: 'flex-end',
              }}>
              <Icon name="close" size={20} color="#000" />
            </TouchableOpacity>

            <View style={{ flexDirection: 'row', alignSelf: 'center', }}>
              <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}>
                Tambah Data
              </Text>

            </View>

            <View
              style={{
                width: '100%',
                padding: 5,
              }}>
              <View>
                <Text style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
                  Nama Mobil
                </Text>
                <TextInput
                  placeholder="Masukkan Nama Mobil"
                  value={namaMobil}
                  onChangeText={text => setNamaMobil(text)}
                  style={styles.txtInput}
                />
              </View>
              <View style={{ marginTop: 20 }}>
                <Text style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
                  Total Kilometer
                </Text>
                <TextInput
                  placeholder="contoh: 100 KM"
                  value={totalKM}
                  onChangeText={text => setTotalKM(text)}
                  style={styles.txtInput}
                />
              </View>
              <View style={{ marginTop: 20 }}>
                <Text style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
                  Harga Mobil
                </Text>
                <TextInput
                  placeholder="Masukkan Harga Mobil"
                  value={hargaMobil}
                  onChangeText={text => setHargaMobil(text)}
                  style={styles.txtInput}
                  keyboardType="number-pad"
                />
              </View>
              <TouchableOpacity
                onPress={postData}
                style={styles.btnAdd}>
                <Text style={{ color: '#fff', fontWeight: '600' }}>
                  Tambah Data
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default Home;
