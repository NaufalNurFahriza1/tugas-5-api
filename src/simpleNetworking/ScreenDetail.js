import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
  ActivityIndicator,
  Alert,
  ImageBackground,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './url';

const UbahData = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  var dataMobil = route.params;
  
  const editData = async () => {
    setLoading(true);
    if (!namaMobil || !totalKM || !hargaMobil) {
      setLoading(false);
      alert('Nama mobil, Total Kilometer dan Harga Mobil tidak boleh');
      return;
    }

    if (hargaMobil < 20000000) {
      setLoading(false);
      alert(
        'Harga mobil tidak boleh kosong dan tidak boleh dibawah 100 juta rupiah',
      );
      return;
    }

    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://gitlab.com/uploads/-/system/user/avatar/3683300/avatar.png?width=400',
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);

      Alert.alert('Alert Title', 'Data Mobil berhasil dirubah', [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => setModalVisible(false)
          // onPress: () => navigation.navigate('Home'),
        },
      ]);
    } catch (error) {
      console.error('Error:', error);
    }
    setLoading(false);
    setTimeout(() => {}, 2000);
  };

  const deleteData = async () => {
    setLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });
      const result = await response.json();
      Alert.alert('Alert Title', 'Data Mobil berhasil dihapus', [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => navigation.navigate('Home')},
      ]);
    } catch (error) {
      console.error('Error:', error);
    }
    setLoading(false);
    setTimeout(() => {}, 2000);
  };

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      console.log('datamobil', data);
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);q
    }
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <Modal animationType="fade" transparent={true} visible={loading}>
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0,0,0,0.2)',
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                padding: 20,
                borderRadius: 18,
                backgroundColor: '#FFFFFF',
              }}>
              <ActivityIndicator size="large" color="#00FF00" />
            </View>
          </View>
        </Modal>

        <View style={{width: '100%'}}>
          <ImageBackground
            // source={require('../assets/image_seturan.png')} //load asset dari local
            source={{uri: dataMobil.unitImage}}
            style={{
              width: '100%',
              height: 316,
            }}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 10,
                }}>
                <Icon name="arrowleft" size={20} color="#fff" />
              </TouchableOpacity>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: '#fff'}}>
                Screen Detail
              </Text>
            </View>
          </ImageBackground>
        </View>
        <View
          style={{
            paddingHorizontal: 20,
            paddingBottom: 14,
            paddingTop: 24,
            marginTop: -30,
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            backgroundColor: '#FFFFFF',
          }}></View>

        <View
          style={{
            width: '100%',
            padding: 15,
          }}>
          <View>
            <Text style={{fontSize: 18, fontWeight: '700', color: '#201F26'}}>
              Detail Mobil
            </Text>
            <Text
              style={{
                color: '#595959',
                marginVertical: 10,
                fontSize: 15,
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
              gravida mattis arcu interdum lectus egestas scelerisque. Blandit
              porttitor diam viverra amet nulla sodales aliquet est. Donec enim
              turpis rhoncus quis integer. Ullamcorper morbi donec tristique
              condimentum ornare imperdiet facilisi pretium molestie.
            </Text>
          </View>
          <View style={{flexDirection:'row', marginVertical:5, }}>
            <Text style={{fontSize: 16, color: '#000', fontWeight: '600', paddingRight:5}}>
              Nama Mobil :
            </Text>
            <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
              {dataMobil.title}
            </Text>
          </View>

          <View style={{flexDirection:'row', marginVertical:5, }}>
            <Text style={{fontSize: 16, color: '#000', fontWeight: '600', paddingRight:5}}>
              Total KM :
            </Text>
            <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
              {dataMobil.totalKM}
            </Text>
          </View>

          <View style={{flexDirection:'row', marginVertical:5,}}>
            <Text style={{fontSize: 16, color: '#000', fontWeight: '600', paddingRight:5}}>
              Harga Mobil :
            </Text>
            <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
              {convertCurrency(dataMobil.harga, 'Rp. ')}
            </Text>
          </View>

          <TouchableOpacity
            // onPress={editData}
            onPress={() => setModalVisible(true)}
            style={styles.btnEdit}>
            <Text style={{color: '#fff', fontWeight: '600'}}>Edit Data</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={deleteData} style={styles.btnDel}>
            <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
          </TouchableOpacity>
        </View>

        <Modal animationType="fade" transparent={true} visible={modalVisible}>
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0,0,0,0.2)',
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                paddingHorizontal: 10,
                paddingVertical: 10,
                borderRadius: 18,
                backgroundColor: '#FFFFFF',
                width: '85%',
                height: '65%',
                alignContent: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => setModalVisible(false)}
                style={{
                  width: '10%',
                  alignSelf: 'flex-end',
                }}>
                <Icon name="close" size={20} color="#000" />
              </TouchableOpacity>

              <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
                  Edit Data
                </Text>
              </View>

              <View
                style={{
                  width: '100%',
                  padding: 5,
                }}>

                <View>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    placeholder="Masukkan Nama Mobil"
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                    style={styles.txtInput}
                  />
                </View>

                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    placeholder="contoh: 100 KM"
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                    style={styles.txtInput}
                  />
                </View>

                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    placeholder="Masukkan Harga Mobil"
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                    style={styles.txtInput}
                    keyboardType="number-pad"
                  />
                </View>

                <TouchableOpacity onPress={editData} style={styles.btnEdit}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>
                    Edit Data
                  </Text>
                </TouchableOpacity>

              </View>
            </View>
          </View>
        </Modal>

      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  btnEdit: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#00A0F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnDel: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#EE5F85',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default UbahData;
