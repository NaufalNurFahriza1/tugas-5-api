import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";


import Home from "./Home";
import AddData from "./AddData";
import ScreenDetail from "./ScreenDetail";
import UbahData from "./ScreenDetail";
import Coba from "./Coba";

const Stack = createNativeStackNavigator();

export default function Routing() {
    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="AddData" component={AddData} />
                <Stack.Screen name="ScreenDetail" component={UbahData} />
                <Stack.Screen name="Coba" component={Coba} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}